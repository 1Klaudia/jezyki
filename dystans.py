__author__ = 'klaudia'

import math

def wypelnienie(n,plik):
    with open(plik) as plik:
        tekst=plik.read()
        lista = []
        if n> 1:
            znacz = " " * (n-1)
            tekst = znacz + tekst + znacz
        dl=len(tekst)
        for i in range( dl - (n - 1) ):
            lista.append(tekst[i:i+n])
        plik.close()




    with open('wynik','a') as pl2:
        for i in lista:
            pl2.write("%s\n"%(i))
        pl2.write("**********************")
        pl2.close()
    return lista



def hamming(s1,s2):
    return sum(c1!=c2 for c1,c2 in zip(s1,s2))




def levenshtein(a,b):
    x,y= len(a),len(b)
    if x> y:

        a,b=b,a
        x,y=y,x

    tmp = range(x+1)
    for i in range(1,y+1):
        pre= tmp
        tmp=[i]+[0]*x
        for i in range(1,x+1):
            ad= pre[i]+1
            dlt=tmp[i-1]+1
            zm = pre[i-1]
            if a[i-1] != b[i-1]:
                zm = zm + 1
            tmp[i] = min(ad, dlt, zm)

    return tmp[x]

Klaudia Biela
