__author__ = 'klaudia'
import os
import ngram
import dystans
import math
from tkinter import *
import sqlite3

con = sqlite3.connect('jezyki.db')
con.row_factory = sqlite3.Row
cur = con.cursor()



cur.executescript("""
    CREATE TABLE IF NOT EXISTS jezyk (
        id INTEGER PRIMARY KEY ASC,
        nazwa varchar(250) NOT NULL,
        ind varchar(250) DEFAULT ''
    )""")

cur.executescript("""
    DROP TABLE IF EXISTS probka;
    CREATE TABLE IF NOT EXISTS probka (
        id INTEGER PRIMARY KEY ASC,
        gram varchar(2500000) NOT NULL,
        ngram varchar(250) NOT NULL,
        jezyk_id INTEGER NOT NULL,
        FOREIGN KEY(jezyk_id) REFERENCES jezyk(id)
    )""")


zbior=['polski2.txt','english2.txt','deutch.txt']

dlugosc=4*len(zbior)
#print(dlugosc)


def wypelnienie(n,plik):
    with open(plik) as plik:
        tekst=plik.read()
        lista = []
        if n> 1:
            znacz = " " * (n-1)
            tekst = znacz + tekst + znacz
        dl=len(tekst)
        for i in range( dl - (n - 1) ):
            lista.append(tekst[i:i+n])






    with open('wynik','a') as pl2:
        for i in lista:
            pl2.write("%s\n"%(i))
        pl2.write("**********************\n")
    return lista



if os.path.exists('/home/klaudia/pycharm/baza/wynik'):
        os.remove('wynik')
for i in zbior:
    for j in range(4):
        wypelnienie(j+1,i)

with open('wynik') as pl:
    dane=pl.read()
    l=dane.split("**********************")




cur.execute('INSERT INTO jezyk VALUES(NULL, ?,?);', ('polski','1'))
cur.execute('INSERT INTO jezyk VALUES(NULL, ?, ?);', ('english','2'))
cur.execute('INSERT INTO jezyk VALUES(NULL, ?, ?);', ('deutch','3'))
cur.execute('SELECT id FROM jezyk WHERE nazwa = ?',('polski',))

jezyk_id = cur.fetchone()[0]



probki= (
    (None,'unigram',l[0],jezyk_id),
    (None,'bigram',l[1],jezyk_id),
    (None,'trigram',l[2],jezyk_id),
    (None,'quadgram',l[3],jezyk_id)
)

cur.executemany('INSERT INTO probka VALUES(?,?,?,?)', probki)
con.commit()

cur.execute('SELECT id FROM jezyk WHERE nazwa = ?',('english',))

jezyk_id = cur.fetchone()[0]


probki2= (
    (None,'unigram',l[4],jezyk_id),
    (None,'bigram',l[5],jezyk_id),
    (None,'trigram',l[6],jezyk_id),
    (None,'quadgram',l[7],jezyk_id)
)
cur.executemany('INSERT INTO probka VALUES(?,?,?,?)', probki2)

con.commit()

cur.execute('SELECT id FROM jezyk WHERE nazwa = ?',('deutch',))

jezyk_id = cur.fetchone()[0]

probki3= (
    (None,'unigram',l[8],jezyk_id),
    (None,'bigram',l[9],jezyk_id),
    (None,'trigram',l[10],jezyk_id),
    (None,'quadgram',l[11],jezyk_id)
)
cur.executemany('INSERT INTO probka VALUES(?,?,?,?)', probki3)
con.commit()



